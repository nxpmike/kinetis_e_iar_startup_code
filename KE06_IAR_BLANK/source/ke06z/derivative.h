#include "MKE06Z4.h"                         // KE06 M0+ register definitions
#include "core_cm0plus.h"                    // KE06 M0+ additional register definitions
#include "arm_cm0.h"                         // ARM data types
#include "system_MKE06Z4.h"