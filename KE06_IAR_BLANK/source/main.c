// INCLUDES /////////////////////////////////////////////////////////                    
//-------------------------------------------------------------------                    
#include "MKE06Z4.h"                         // KE06 M0+ register definitions


// DEFINES //////////////////////////////////////////////////////////                    
//-------------------------------------------------------------------                    
#define Pot          0x0B                    // Potentiometer is on A/D 0 Channel 11


// GLOBAL CONSTANTS /////////////////////////////////////////////////                    
//-------------------------------------------------------------------                    
#undef  WDOG_CNT
#undef  WDOG_TOVAL
#undef  WDOG_WIN
#define WDOG_CNT          *((volatile short*)&WDOG_CNTH)
#define WDOG_TOVAL        *((volatile short*)&WDOG_TOVALH)
#define WDOG_WIN          *((volatile short*)&WDOG_WINH)


// GLOBAL VARIABLES /////////////////////////////////////////////////                    
//-------------------------------------------------------------------                    
uint32_t adcin;                                  // adcin will store ADC results


// FUNCTION HEADERS /////////////////////////////////////////////////                    
//-------------------------------------------------------------------                    
void MCU_Init(void);                         // initializes MCU for Freedom Board
void MCU_Delay (uint32_t delay);             // Delay in multiples of 1ms (e.g. use 1000 for 1 second)
//---------------------------------------------------------------------
void RGB(uint8_t Red,uint8_t Green,uint8_t Blue);        // RGB-LED Control: 1=on, 0=off, for each of the 3 colors
uint32_t ADC_Read(uint8_t chan);                     // Analog-to-Digital Converter byte read: enter channel to read


// *** MAIN *********************************************************                    
//-------------------------------------------------------------------                    
int main(void)                                                                           
{
MCU_Init();                                  // MCU Initialization; has to be done prior to anything else
RGB(0,0,0);                                  // Start with all LEDs off
for(;;)                                      // forever loop 
  { 
   RGB(1,0,0);                               // Turn Red LED On
   MCU_Delay(100);                           // Delay 100ms
   RGB(0,0,0);                               // Turn Red LED Off
   MCU_Delay(100);                           // Delay 100ms
  } 
// return 0;                                                                             
}                                                                                        
//-------------------------------------------------------------------                    
// ******************************************************************                    


// FUNCTION BODIES //////////////////////////////////////////////////                    
//-------------------------------------------------------------------                    
void MCU_Init(void) 
{
// Disable Interrupts                       
__asm("CPSID i");                            // Disable interrupts

WDOG->CNT = 0x20C5; 
WDOG->CNT = 0x28D9;  
WDOG->CS2 = 0;
WDOG->TOVAL = 0xFFFF;
WDOG->WIN = 0;
WDOG->CS1 = 0x20;                             // WDOGA = 1 to allow reconfigure watchdog at any time by executing an unlock sequence        

//System Registers 
SIM->SCGC|=SIM_SCGC_PIT_MASK;                 // Enable PIT module
SIM->SCGC|=SIM_SCGC_FTM0_MASK;                // Enable FTM0 module 
SIM->SCGC|=SIM_SCGC_ADC_MASK;                 // Enable ADC module
SIM->SCGC|=SIM_SCGC_UART1_MASK;               // Enable UART0 module  
SIM->SCGC|=SIM_SCGC_ACMP0_MASK;               // Enable ACMP0 module  

// System Tick Init
SysTick->CTRL=0;                             // Disable the SysTick Timer
SysTick->LOAD=24000;                         // Busclk/1=37500x1280=48,000,000 /2  Period=1ms/(1/24000000Hz)=24000
SysTick->VAL=0;                              // Clear the current counter value to 0
SysTick->CTRL=(SysTick_CTRL_ENABLE_Msk
             |SysTick_CTRL_CLKSOURCE_Msk);   // Enable SYS TICK timer and set clock source to processor clock (core clock)

//System clock initialization
ICS->C1 = 0x04;                               // INTERNAL BUS CLK
ICS->C2 = 0x00;                               // FULL SPEED BUS CLK, busclk=32.768KHz*1024=20971520Hz/2=16.777MHz 
ICS->C4 = 0x00;                               //    

// Modulo Timer Init
PIT->MCR=0x00;                                // MDIS = 0,  enables timer
PIT->CHANNEL[0].LDVAL=16777;                  // PIT Period=1ms,  busclk=32.768KHz*1024=20971520Hz/2=16777216Hz, Period=1ms/(1/16777216Hz)=16777.2 
PIT->CHANNEL[0].TCTRL=0x0001;                 // Enable PIT1 polling

// Timer2 Init
FTM0->SC=0x00;                                // busclk/1=59.6ns per count; buzzer on=0x08, off=0x00
FTM0->CONTROLS[0].CnSC=0x28;                              // edge-aligned PWM
FTM0->MOD=16777;                              // 1KHz by default
FTM0->CONTROLS[0].CnV=16777>>1;                           // half of the above to produce 50% duty cycle PWM

// GPIO Init
GPIOB->PDDR|=(1<<21);                         // Red LED, Negative Logic (0=on, 1=off)
GPIOB->PDDR|=(1<<22);                         // Green LED, Negative Logic (0=on, 1=off)
GPIOB->PDDR|=(1<<23);                         // Blue LED, Negative Logic (0=on, 1=off)
}
//---------------------------------------------------------------------
void MCU_Delay (uint32_t delay)              // Delay in multiples of 1ms (e.g. use 1000 for 1 second)
{uint32_t delw; for (delw=0;delw<delay;delw++) {while (!(SysTick->CTRL&SysTick_CTRL_COUNTFLAG_Msk));}}
//---------------------------------------------------------------------
void RGB(uint8_t Red,uint8_t Green,uint8_t Blue)         // RGB-LED Control: 1=on, 0=off, for each of the 3 colors
{
if (Red   ==1) GPIOB->PCOR|=(1<<21); else GPIOB->PSOR|=(1<<21);
if (Green ==1) GPIOB->PCOR|=(1<<22); else GPIOB->PSOR|=(1<<22);
if (Blue  ==1) GPIOB->PCOR|=(1<<23); else GPIOB->PSOR|=(1<<23);
}
//---------------------------------------------------------------------
uint32_t ADC_Read(uint8_t chan)                      // Analog-to-Digital Converter byte read: enter channel to read
{ADC->SC1=chan; while ((ADC->SC1&ADC_SC1_COCO_MASK)==0); return ADC->R;}
//---------------------------------------------------------------------


// INTERRUPT BODIES /////////////////////////////////////////////////                    
//-------------------------------------------------------------------                    


