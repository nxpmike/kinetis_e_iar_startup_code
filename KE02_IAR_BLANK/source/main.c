// INCLUDES /////////////////////////////////////////////////////////                    
//-------------------------------------------------------------------                    
#include "MKE02Z2.h"                      // KE02 M0+ register definitions


// DEFINES //////////////////////////////////////////////////////////                    
//-------------------------------------------------------------------                    
#define Pot          0x0B                    // Potentiometer is on A/D 0 Channel 11
#define PIT_irq     22                       // PIT0 irq is the Vector#-16 = 38-16=22


// GLOBAL CONSTANTS /////////////////////////////////////////////////                    
//-------------------------------------------------------------------                    
#undef  WDOG_CNT
#undef  WDOG_TOVAL
#undef  WDOG_WIN
#define WDOG_CNT          *((volatile short*)&WDOG_CNTH)
#define WDOG_TOVAL        *((volatile short*)&WDOG_TOVALH)
#define WDOG_WIN          *((volatile short*)&WDOG_WINH)


// GLOBAL VARIABLES /////////////////////////////////////////////////                    
//-------------------------------------------------------------------                    
uint32_t adcin;                              // adcin will store ADC results


// FUNCTION HEADERS /////////////////////////////////////////////////                    
//-------------------------------------------------------------------                    
void MCU_Init(void);                         // initializes MCU for Freedom Board
void MCU_Delay (uint32_t delay);             // Delay in multiples of 1ms (e.g. use 1000 for 1 second)
//---------------------------------------------------------------------
void RGB(uint8_t Red,uint8_t Green,uint8_t Blue);        // RGB-LED Control: 1=on, 0=off, for each of the 3 colors
uint32_t ADC_Read(uint8_t chan);                     // Analog-to-Digital Converter byte read: enter channel to read


// *** MAIN *********************************************************                    
//-------------------------------------------------------------------                    
int main(void)                                                                           
{

///////////////////////////////////////////////
// IF USING IAR:                             // 
// Add additional code located in vectors.c  //
// #undef  VECTOR_038                        //
// #define VECTOR_038 PIT_CH0_IRQHandler     //         
//                                           //
// Add additional code located in vectors.h  //
// void PIT_CH0_IRQHandler(void);            //
///////////////////////////////////////////////

MCU_Init();                                  // MCU Initialization; has to be done prior to anything else
RGB(0,0,0);                                  // Start with all LEDs off

__asm("CPSIE i");                            // Enable all Interrupts
PIT_MCR=0x00;                                // MDIS = 0,  enables timer
PIT_LDVAL0=2097152;                          // PIT Period=100ms, busclk=32.768KHz*640=20971520Hz, Period=100ms/(1/20971520Hz)=2097152 
PIT_TCTRL0 = 0x0003;                         // Enable PIT1 Timer and interrupts
#if defined(IAR)
NVIC_ICPR |= 1 << (PIT_irq%32);              // Clear pending PIT interrupts in NVIC register
NVIC_ISER |= 1 << (PIT_irq%32);              // Enable PIT IRQ in NVIC register
#else
NVIC->ISER[0] = (1 << (PIT_irq & 0x1F));     // Set PIT Interrupt in NVIC Core register 
NVIC->ICPR[0] = (1 << (PIT_irq & 0x1F));     // Clear PIT Interrupt in NVIC Core register
#endif


for(;;);                                     // forever loop 
// return 0;                                                                             
}                                                                                        
//-------------------------------------------------------------------                    
// ******************************************************************                    


// FUNCTION BODIES //////////////////////////////////////////////////                    
//-------------------------------------------------------------------                    
void MCU_Init(void) 
{
// Disable Interrupts                       
__asm("CPSID i");                            // Disable interrupts

//Disable Watchdog
WDOG_CNT = 0x20C5; 
WDOG_CNT = 0x28D9;  
WDOG_CS2 = 0;
WDOG_TOVAL = 0xFFFF;
WDOG_WIN = 0;
WDOG_CS1 = 0x20;                             // WDOGA = 1 to allow reconfigure watchdog at any time by executing an unlock sequence        

//System Registers 
SIM_BUSDIV=0x00000001;                       // Bus clock /2, this is needed for the KE02, Bus limit 20MHz
SIM_SCGC|=SIM_SCGC_PIT_MASK;                 // Enable PIT module
SIM_SCGC|=SIM_SCGC_FTM0_MASK;                // Enable FTM0 module 
SIM_SCGC|=SIM_SCGC_ADC_MASK;                 // Enable ADC module
SIM_SCGC|=SIM_SCGC_UART1_MASK;               // Enable UART0 module  
SIM_SCGC|=SIM_SCGC_ACMP0_MASK;               // Enable ACMP0 module  

// System Tick Init
#if defined(IAR)
SYST_CSR=0x00000000;                         // Disable the SysTick Timer
SYST_RVR=SysTick_RVR_RELOAD(20972);          // Busclk/1=32.768KHz*640=20971520Hz, Period=1ms/(1/20971520Hz)=20972
SYST_CVR=SysTick_CVR_CURRENT(0x00);          // Clear the current counter value to 0
SYST_CSR=(SysTick_CSR_ENABLE_MASK
           |SysTick_CSR_CLKSOURCE_MASK);     // Enable SYS TICK timer and set clock source to processor clock (core clock)
#else
SysTick->CTRL=0;                             // Disable the SysTick Timer
SysTick->LOAD=20972;                         // Core Clock/1=48MHz, Period=1ms/(1/48000000Hz)=48000
SysTick->VAL=0;                              // Clear the current counter value to 0
SysTick->CTRL=(SysTick_CTRL_ENABLE_Msk
             |SysTick_CTRL_CLKSOURCE_Msk);   // Enable SYS TICK timer and set clock source to processor clock (core clock)
#endif

//System clock initialization
ICS_C1 = 0x04;                               // INTERNAL BUS CLK
ICS_C2 = 0x00;                               // FULL SPEED BUS CLK, busclk=32.768KHz*1024=20971520Hz/2=16.777MHz 
ICS_C4 = 0x00;                               //    

// ADC Init
ADC_SC3=0x48;                                // 12 bits & 4.77MHz clock (/4)

// Modulo Timer Init
PIT_MCR=0x00;                                // MDIS = 0,  enables timer
PIT_LDVAL1=16777;                            // PIT Period=1ms,  busclk=32.768KHz*1024=20971520Hz/2=16777216Hz, Period=1ms/(1/16777216Hz)=16777.2 
PIT_TCTRL1=0x0001;                           // Enable PIT1 polling

// Timer2 Init
FTM0_SC=0x00;                                // busclk/1=59.6ns per count; buzzer on=0x08, off=0x00
FTM0_C0SC=0x28;                              // edge-aligned PWM
FTM0_MOD=16777;                              // 1KHz by default
FTM0_C0V=16777>>1;                           // half of the above to produce 50% duty cycle PWM

// GPIO Init
GPIOB_PDDR|=(1<<25);                         // Red LED, Negative Logic (0=on, 1=off)
GPIOB_PDDR|=(1<<26);                         // Green LED, Negative Logic (0=on, 1=off)
GPIOB_PDDR|=(1<<7);                          // Blue LED, Negative Logic (0=on, 1=off)
}
//---------------------------------------------------------------------
void MCU_Delay (uint32_t delay)              // Delay in multiples of 1ms (e.g. use 1000 for 1 second)
#if defined(IAR)
{uint32_t delw; for (delw=0;delw<delay;delw++) {while (!(SYST_CSR&SysTick_CSR_COUNTFLAG_MASK));}}
#else
{uint32_t delw; for (delw=0;delw<delay;delw++) {while (!(SysTick->CTRL&SysTick_CTRL_COUNTFLAG_Msk));}}
#endif
//---------------------------------------------------------------------
void RGB(uint8_t Red,uint8_t Green,uint8_t Blue)         // RGB-LED Control: 1=on, 0=off, for each of the 3 colors
{
if (Red   ==1) GPIOB_PCOR|=(1<<25); else GPIOB_PSOR|=(1<<25);
if (Green ==1) GPIOB_PCOR|=(1<<26); else GPIOB_PSOR|=(1<<26);
if (Blue  ==1) GPIOB_PCOR|=(1<<7);  else GPIOB_PSOR|=(1<<7);
}
//---------------------------------------------------------------------
uint32_t ADC_Read(uint8_t chan)                      // Analog-to-Digital Converter byte read: enter channel to read
{ADC_SC1=chan; while ((ADC_SC1&ADC_SC1_COCO_MASK)==0); return ADC_R;}
//---------------------------------------------------------------------


// INTERRUPT BODIES /////////////////////////////////////////////////                    
//-------------------------------------------------------------------                    
//PIT0 Interrupt Service Routine
void PIT_CH0_IRQHandler(void)
{
  PIT_TFLG0 |= PIT_TFLG_TIF_MASK;            // Clear PIT interrupt flag
  GPIOB_PTOR|=(1<<26);                       // Toggle GREEN LED
}


